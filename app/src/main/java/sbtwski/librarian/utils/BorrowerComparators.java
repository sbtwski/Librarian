package sbtwski.librarian.utils;

import java.util.Comparator;
import java.util.HashMap;

import sbtwski.librarian.entities.BookBorrower;
import static sbtwski.librarian.utils.Constants.*;

public class BorrowerComparators {
    private static final Comparator<BookBorrower> NAME_ASC = (BookBorrower o1, BookBorrower o2) -> o1.getName().compareTo(o2.getName());
    private static final Comparator<BookBorrower> NAME_DESC = (BookBorrower o1, BookBorrower o2) -> o2.getName().compareTo(o1.getName());
    private static final Comparator<BookBorrower> SURNAME_ASC = (BookBorrower o1, BookBorrower o2) -> o1.getSurname().compareTo(o2.getSurname());
    private static final Comparator<BookBorrower> SURNAME_DESC = (BookBorrower o1, BookBorrower o2) -> o2.getSurname().compareTo(o1.getSurname());
    private static final Comparator<BookBorrower> CURRENTLY_BORROWED_ASC = (BookBorrower o1, BookBorrower o2) -> o1.getCurrentlyBorrowed().compareTo(o2.getCurrentlyBorrowed());
    private static final Comparator<BookBorrower> CURRENTLY_BORROWED_DESC = (BookBorrower o1, BookBorrower o2) -> o2.getCurrentlyBorrowed().compareTo(o1.getCurrentlyBorrowed());
    private static final Comparator<BookBorrower> ALL_BORROWED_ASC = (BookBorrower o1, BookBorrower o2) -> o1.getAllBorrowed().compareTo(o2.getAllBorrowed());
    private static final Comparator<BookBorrower> ALL_BORROWED_DESC = (BookBorrower o1, BookBorrower o2) -> o2.getAllBorrowed().compareTo(o1.getAllBorrowed());

    public static final HashMap<Integer,Comparator<BookBorrower>> COMPARATORS_MAP = new HashMap<Integer, Comparator<BookBorrower>>(){{
        put(BORROWER_NAME_ASC, NAME_ASC);
        put(BORROWER_NAME_DESC, NAME_DESC);
        put(BORROWER_SURNAME_ASC, SURNAME_ASC);
        put(BORROWER_SURNAME_DESC, SURNAME_DESC);
        put(BORROWER_CURRENT_BORROWINGS_ASC, CURRENTLY_BORROWED_ASC);
        put(BORROWER_CURRENT_BORROWINGS_DESC, CURRENTLY_BORROWED_DESC);
        put(BORROWER_ALL_BORROWINGS_ASC, ALL_BORROWED_ASC);
        put(BORROWER_ALL_BORROWINGS_DESC, ALL_BORROWED_DESC);
    }};
}
