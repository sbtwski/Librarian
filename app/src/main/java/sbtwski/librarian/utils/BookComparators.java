package sbtwski.librarian.utils;

import java.util.Comparator;
import java.util.HashMap;

import sbtwski.librarian.entities.BorrowedBook;
import static sbtwski.librarian.utils.Constants.*;

public class BookComparators {
    private static final Comparator<BorrowedBook> TITLE_ASC = (BorrowedBook o1, BorrowedBook o2) -> o1.getTitle().compareTo(o2.getTitle());
    private static final Comparator<BorrowedBook> TITLE_DESC = (BorrowedBook o1, BorrowedBook o2) -> o2.getTitle().compareTo(o1.getTitle());
    private static final Comparator<BorrowedBook> AUTHOR_ASC = (BorrowedBook o1, BorrowedBook o2) -> o1.getAuthor().compareTo(o2.getAuthor());
    private static final Comparator<BorrowedBook> AUTHOR_DESC = (BorrowedBook o1, BorrowedBook o2) -> o2.getAuthor().compareTo(o1.getAuthor());
    private static final Comparator<BorrowedBook> BOOK_BORROWED_ASC = (BorrowedBook o1, BorrowedBook o2) -> o1.getAllBorrowings().compareTo(o2.getAllBorrowings());
    private static final Comparator<BorrowedBook> BOOK_BORROWED_DESC = (BorrowedBook o1, BorrowedBook o2) -> o2.getAllBorrowings().compareTo(o1.getAllBorrowings());

    private static final Comparator<BorrowedBook> RETURNED_FIRST = (BorrowedBook o1, BorrowedBook o2) -> {
        Long time1 = o1.getBorrowTime();
        Long time2 = o2.getBorrowTime();

        if(time1 != null || time2 != null)
            if(time1 != null && time2 != null)
                return time1.compareTo(time2);
            else
                if(time1 == null)
                    return -1;
                else
                    return 1;
        else
            return 0;
    };

    private static final Comparator<BorrowedBook> RETURNED_LAST = (BorrowedBook o1, BorrowedBook o2) -> {
        Long time1 = o1.getBorrowTime();
        Long time2 = o2.getBorrowTime();

        if(time1 != null || time2 != null)
            if(time1 != null && time2 != null)
                return time2.compareTo(time1);
            else
                if(time1 == null)
                    return 1;
                else
                    return -1;
        else
            return 0;
    };

    public static final HashMap<Integer, Comparator<BorrowedBook>> COMPARATORS_MAP = new HashMap<Integer, Comparator<BorrowedBook>>(){{
        put(BOOK_AUTHOR_ASC, AUTHOR_ASC);
        put(BOOK_AUTHOR_DESC, AUTHOR_DESC);
        put(BOOK_TITLE_ASC, TITLE_ASC);
        put(BOOK_TITLE_DESC, TITLE_DESC);
        put(BOOK_BORROWINGS_ASC, BOOK_BORROWED_ASC);
        put(BOOK_BORROWINGS_DESC, BOOK_BORROWED_DESC);
        put(BOOKS_RETURNED_FIRST, RETURNED_FIRST);
        put(BOOKS_RETURNED_LAST, RETURNED_LAST);
    }};
}
