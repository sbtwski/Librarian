package sbtwski.librarian.utils;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class TimeFormattingUtils {

	private static final DateTimeFormatter timeFormatter = DateTimeFormat.forPattern("yyyy.MM.dd");

	public static String format(DateTime dateTime) {
		return timeFormatter.print(dateTime);
	}

	public static String formatPeriod(DateTime from, DateTime to) {
		StringBuilder sb = new StringBuilder();
		sb.append(format(from));
		sb.append(" - ");

		if (to != null) {
			sb.append(format(to));
		}
		else{
			sb.append(" ...");
		}

		return sb.toString();
	}
}
