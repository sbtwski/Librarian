package sbtwski.librarian.utils;

import android.view.animation.AlphaAnimation;

import com.google.common.collect.BiMap;
import com.google.common.collect.ImmutableBiMap;

import sbtwski.librarian.R;

public class Constants {
    public static final String SP_NAME = "librarianSETT";

    public static final String SWIPE_CODE = "swipeType";
    public static final String LONG_PRESS_CODE = "longPress";
    public static final String BOOK_SORT_CODE = "bookSort";
    public static final String BORROWER_SORT_CODE = "borrowerSort";

    static final int BOOK_AUTHOR_ASC = 1;
    static final int BOOK_AUTHOR_DESC = 2;
    static final int BOOK_TITLE_ASC = 3;
    static final int BOOK_TITLE_DESC = 4;
    static final int BOOK_BORROWINGS_ASC = 5;
    static final int BOOK_BORROWINGS_DESC = 6;
    static final int BOOKS_RETURNED_FIRST = 7;
    static final int BOOKS_RETURNED_LAST = 8;

    static final int BORROWER_NAME_ASC = 9;
    static final int BORROWER_NAME_DESC = 10;
    static final int BORROWER_SURNAME_ASC = 11;
    static final int BORROWER_SURNAME_DESC = 12;
    static final int BORROWER_CURRENT_BORROWINGS_ASC = 13;
    static final int BORROWER_CURRENT_BORROWINGS_DESC = 14;
    static final int BORROWER_ALL_BORROWINGS_ASC = 15;
    static final int BORROWER_ALL_BORROWINGS_DESC = 16;

    public static final BiMap<Integer,Integer> BOOK_RADIO_BUTTONS_MAP = new ImmutableBiMap.Builder<Integer, Integer>()
        .put(R.id.book_author_az, BOOK_AUTHOR_ASC)
        .put(R.id.book_author_za, BOOK_AUTHOR_DESC)
        .put(R.id.book_title_az, BOOK_TITLE_ASC)
        .put(R.id.book_title_za, BOOK_TITLE_DESC)
        .put(R.id.book_borrowings_ascending, BOOK_BORROWINGS_ASC)
        .put(R.id.book_borrowings_descending, BOOK_BORROWINGS_DESC)
        .put(R.id.books_returned_first, BOOKS_RETURNED_FIRST)
        .put(R.id.books_returned_last, BOOKS_RETURNED_LAST)
        .build();

    public static final BiMap<Integer,Integer> BORROWER_RADIO_BUTTONS_MAP = new ImmutableBiMap.Builder<Integer, Integer>()
        .put(R.id.borrower_name_az, BORROWER_NAME_ASC)
        .put(R.id.borrower_name_za, BORROWER_NAME_DESC)
        .put(R.id.borrower_surname_az, BORROWER_SURNAME_ASC)
        .put(R.id.borrower_surname_za, BORROWER_SURNAME_DESC)
        .put(R.id.borrower_current_borrowings_ascending, BORROWER_CURRENT_BORROWINGS_ASC)
        .put(R.id.borrower_current_borrowings_descending, BORROWER_CURRENT_BORROWINGS_DESC)
        .put(R.id.borrower_all_borrowings_ascending, BORROWER_ALL_BORROWINGS_ASC)
        .put(R.id.borrower_all_borrowings_descending, BORROWER_ALL_BORROWINGS_DESC)
        .build();

    public static final AlphaAnimation buttonClick = new AlphaAnimation(0.5F, 1F){{
        setDuration(400);
    }};
}
