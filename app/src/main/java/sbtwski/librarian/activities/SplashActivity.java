package sbtwski.librarian.activities;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;

import static java.lang.Thread.sleep;

public class SplashActivity extends AppCompatActivity {

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		new Thread(() -> {
			try {
				sleep(350);
			} catch (Exception e) {
				Log.e("SplashThread", "Interrupted");
			} finally {
				startActivity(new Intent(SplashActivity.this, MainActivity.class));
				finish();
			}
		}).start();
	}
}
