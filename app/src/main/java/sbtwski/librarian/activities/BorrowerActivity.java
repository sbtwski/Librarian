package sbtwski.librarian.activities;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;

import android.view.View;
import android.widget.TextView;

import java.util.List;

import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import sbtwski.librarian.R;
import sbtwski.librarian.activities.base.BaseToolbarActivity;
import sbtwski.librarian.adapters.BorrowerPreviewAdapter;
import sbtwski.librarian.entities.BorrowedBook;
import sbtwski.librarian.entities.Borrower;
import sbtwski.librarian.viewmodels.BorrowingViewModel;

public class BorrowerActivity extends BaseToolbarActivity {

	@BindView(R.id.borrower_data_borrower_preview) TextView borrowerData;

	private Borrower borrower;
	private BorrowerPreviewAdapter adapter;
	private BorrowingViewModel borrowingViewModel;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		inflateContent(R.layout.activity_borrower);
        ButterKnife.bind(this);

		borrower = (Borrower) getIntent().getSerializableExtra("borrower");
		getData();

		setupCustomButton(R.drawable.ic_back, new BorrowerActivityFinishListener());
        adapter = new BorrowerPreviewAdapter();

        RecyclerView borrowersPreview = findViewById(R.id.borrower_preview_recycler);
        borrowersPreview.setLayoutManager(new LinearLayoutManager(this));
        borrowersPreview.setAdapter(adapter);

		fillDataFields();
	}

	private void fillDataFields() {
		borrowerData.setText(borrower.toString());
	}

	private void getData(){
        borrowingViewModel = ViewModelProviders.of(this).get(BorrowingViewModel.class);
        borrowingViewModel.getBorrowedBooks(borrower).observe(this, (@Nullable final List<BorrowedBook> books) -> adapter.setDatabase(books));
	}

	private class BorrowerActivityFinishListener implements Toolbar.OnClickListener {
        @Override
        public void onClick(View v) {
            removeObserver();

            Intent toMain = new Intent(getApplicationContext(), MainActivity.class);
            toMain.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(toMain);
        }
    }

    private void removeObserver(){
        borrowingViewModel.getBorrowedBooks(borrower).removeObservers(this);
    }
}
