package sbtwski.librarian.activities.base;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import sbtwski.librarian.R;

public class SimpleToolbarActivity extends AppCompatActivity {
    private Toolbar baseToolbar;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_base);

		baseToolbar = findViewById(R.id.base_toolbar);
	}

    protected void setupBackButton(){
        baseToolbar.setNavigationIcon(getDrawable(R.drawable.ic_back));
        baseToolbar.setNavigationOnClickListener(v -> finish());
    }

    protected void setupCustomButton(Toolbar.OnClickListener listener) {
        baseToolbar.setNavigationIcon(getDrawable(R.drawable.ic_back));
        baseToolbar.setNavigationOnClickListener(listener);
    }

	protected void inflateContent(int layoutID){
		LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		ViewGroup parent = findViewById(R.id.content_frame);
		View inflated = inflater.inflate(layoutID, parent, false);
		parent.addView(inflated);
	}
}
