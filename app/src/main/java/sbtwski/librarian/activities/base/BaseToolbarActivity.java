package sbtwski.librarian.activities.base;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import sbtwski.librarian.R;
import sbtwski.librarian.activities.AboutActivity;
import sbtwski.librarian.activities.SettingsActivity;

public class BaseToolbarActivity extends AppCompatActivity {
    private Toolbar baseToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);
        baseToolbar = findViewById(R.id.base_toolbar);
        setupMenu();
    }

    private void setupMenu() {
        baseToolbar.inflateMenu(R.menu.base_menu);
        baseToolbar.setOnMenuItemClickListener((MenuItem item) -> {
            if (item.getItemId() == R.id.settings_menu) {
                startActivity(new Intent(this, SettingsActivity.class));
            }

            if (item.getItemId() == R.id.about_menu) {
                startActivity(new Intent(this, AboutActivity.class));
            }
            return false;
        });
    }

    protected void inflateContent(int layoutID){
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ViewGroup parent = findViewById(R.id.content_frame);
        View inflated = inflater.inflate(layoutID, parent, false);
        parent.addView(inflated);
    }

    protected void setupCustomButton(int iconID, Toolbar.OnClickListener listener) {
        baseToolbar.setNavigationIcon(getDrawable(iconID));
        baseToolbar.setNavigationOnClickListener(listener);
    }
}
