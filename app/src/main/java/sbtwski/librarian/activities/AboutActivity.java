package sbtwski.librarian.activities;

import android.os.Bundle;
import androidx.annotation.Nullable;

import sbtwski.librarian.R;
import sbtwski.librarian.activities.base.SimpleToolbarActivity;

public class AboutActivity extends SimpleToolbarActivity {

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		inflateContent(R.layout.activity_about);
		setupBackButton();
	}
}
