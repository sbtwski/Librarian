package sbtwski.librarian.activities;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import org.joda.time.DateTime;
import org.joda.time.DateTimeComparator;

import java.util.List;

import androidx.lifecycle.ViewModelProviders;
import butterknife.BindView;
import butterknife.ButterKnife;
import sbtwski.librarian.R;
import sbtwski.librarian.activities.base.BaseToolbarActivity;
import sbtwski.librarian.adapters.TabAdapter;
import sbtwski.librarian.entities.Book;
import sbtwski.librarian.entities.BookBorrower;
import sbtwski.librarian.entities.BorrowPeriod;
import sbtwski.librarian.entities.BorrowedBook;
import sbtwski.librarian.entities.Borrower;
import sbtwski.librarian.entities.Borrowing;
import sbtwski.librarian.fragments.DatePickerFragment;
import sbtwski.librarian.handlers.MainClickHandler;
import sbtwski.librarian.viewmodels.BookViewModel;
import sbtwski.librarian.viewmodels.BorrowerViewModel;
import sbtwski.librarian.swipe.CustomViewPager;
import sbtwski.librarian.viewmodels.BorrowingViewModel;

public class MainActivity extends BaseToolbarActivity implements DatePickerDialog.OnDateSetListener, MainClickHandler {

    @BindView(R.id.add_button) FloatingActionButton addButton;
    @BindView(R.id.tab_layout) TabLayout tabLayout;

    private BookViewModel bookViewModel;
    private BorrowerViewModel borrowerViewModel;
    private BorrowingViewModel borrowingViewModel;
    private CustomViewPager viewPager;

    private BorrowedBook returning;
    private final int SNACK_BAR_DURATION = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        inflateContent(R.layout.activity_main);
        ButterKnife.bind(this);

        bookViewModel = ViewModelProviders.of(this).get(BookViewModel.class);
        borrowerViewModel = ViewModelProviders.of(this).get(BorrowerViewModel.class);
        borrowingViewModel = ViewModelProviders.of(this).get(BorrowingViewModel.class);

        setupTabs();

        boolean bookTab = getIntent().getBooleanExtra("bookTAB", false);
        if(bookTab)
            viewPager.setCurrentItem(1);

        setupListeners();
    }

    @Override
    public void borrowerClicked(Long borrowerID) {
        borrowerViewModel.getByID(borrowerID).observe(this, (@Nullable Borrower toPreview) ->{
            if(toPreview != null) {
                Intent toBorrower = new Intent(this, BorrowerActivity.class);
                toBorrower.putExtra("borrower", toPreview);
                startActivity(toBorrower);
            }
        });
    }

    @Override
    public void bookClicked(Long bookID) {
        bookViewModel.getByID(bookID).observe(this, (@Nullable Book toPreview) -> {
            if(toPreview != null) {
                Intent toBook = new Intent(this, BookActivity.class);
                toBook.putExtra("book", toPreview);
                startActivity(toBook);
            }
        });
    }

    @Override
    public void bookReturn(BorrowedBook toReturn) {
        returning = toReturn;
        datePicker();
    }

    @Override
    public void bookEdit(BorrowedBook toEdit) {
        buildBookDialog(toEdit);
    }

    @Override
    public void borrowerEdit(BookBorrower toEdit) {
        buildBorrowerDialog(toEdit);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int day) {
        DateTime dateTime = new DateTime(year, month+1, day, 0, 0);
        checkReturnDate(dateTime);
    }

    private void datePicker() {
        DatePickerFragment fragment = new DatePickerFragment();
        Bundle infoForDialog = new Bundle();
        infoForDialog.putLong("limit", returning.getBorrowTime());
        fragment.setArguments(infoForDialog);
        fragment.show(getSupportFragmentManager(), "main_pick");
    }

    private void checkReturnDate(DateTime returnInput){
        Book forSearch = new Book(returning.getTitle(),returning.getAuthor());
        borrowingViewModel.getBookBorrowPeriods(forSearch).observe(this, (@Nullable List<BorrowPeriod> periods) -> {
            if(periods != null) {
                if (noOverlappingPeriods(periods, returnInput)) {
                    updateBorrowing(returnInput);
                    borrowingViewModel.getBookBorrowPeriods(forSearch).removeObservers(this);
                }
                else {
                    Toast.makeText(getApplicationContext(), getText(R.string.book_already_borrowed), Toast.LENGTH_LONG).show();
                    datePicker();
                }
            }
        });
    }

    private boolean noOverlappingPeriods(List<BorrowPeriod> periods, DateTime returnDate){
        boolean noOverlapping = true;
        DateTimeComparator comparator = DateTimeComparator.getDateOnlyInstance();

        for(BorrowPeriod current: periods){
            DateTime currentBorrowTime = current.getBorrowDateTime();
            if(comparator.compare(returning.getBorrowDateTime(),currentBorrowTime) < 0)
                if(comparator.compare(returnDate, currentBorrowTime) > 0) {
                    noOverlapping = false;
                    break;
                }
        }
        return noOverlapping;
    }

    private void updateBorrowing(DateTime returnInput){
        borrowingViewModel.getBookBorrowing(returning.getId()).observe(this, (@Nullable Borrowing borrowing) ->{
            if(borrowing != null) {
                borrowing.setReturnTime(returnInput.getMillis());
                borrowingViewModel.update(borrowing);
            }
        });
    }

    private void setupListeners() {
        addButton.setOnClickListener(v -> {
            Intent toAdd = new Intent(getApplicationContext(), AddActivity.class);
            startActivityForResult(toAdd, 0);
        });
    }

    private void setupTabs() {
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.borrowers_tab)));
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.books_tab)));

        viewPager = findViewById(R.id.pager);
        viewPager.setSwipingEnabled(false);
        TabAdapter tabAdapter = new TabAdapter(getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(tabAdapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {}

            @Override
            public void onTabReselected(TabLayout.Tab tab) {}
        });

    }

    private void buildBorrowerDialog(final BookBorrower toEdit) {
        LayoutInflater inflater = LayoutInflater.from(this);
        ViewGroup parent = findViewById(R.id.main_frame);
        final View dialogView = inflater.inflate(R.layout.borrower_edit_dialog, parent,false);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MainActivity.this);
        alertDialogBuilder.setView(dialogView);

		final EditText nameInput = dialogView.findViewById(R.id.dialog_name_input);
		nameInput.setText(toEdit.getName());
		final EditText surnameInput = dialogView.findViewById(R.id.dialog_surname_input);
		surnameInput.setText(toEdit.getSurname());
        Button confirmEdit = dialogView.findViewById(R.id.borrower_edit_confirm_button);
        Button rejectEdit = dialogView.findViewById(R.id.borrower_edit_cancel_button);

        final AlertDialog dialogMade = alertDialogBuilder.create();
        if(dialogMade.getWindow() != null)
            dialogMade.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialogMade.show();

        confirmEdit.setOnClickListener(v -> {
            boolean correct = true;

			String name = nameInput.getText().toString().trim();
			String surname = surnameInput.getText().toString().trim();

			if (name.isEmpty()) {
				Snackbar snackBar = Snackbar.make(dialogView, getText(R.string.snack_bar_name), SNACK_BAR_DURATION);
				snackBar.show();
				correct = false;
			}

			if (correct && surname.isEmpty()) {
				Snackbar snackBar = Snackbar.make(dialogView, getText(R.string.snack_bar_surname), SNACK_BAR_DURATION);
				snackBar.show();
				correct = false;
			}

			if (correct) {
				Borrower updated = new Borrower(name, surname);
				updated.setId(toEdit.getId());
				borrowerViewModel.updateRow(updated);
				dialogMade.dismiss();
			}
        });

        rejectEdit.setOnClickListener(v -> dialogMade.cancel());
    }

    private void buildBookDialog(final BorrowedBook toEdit) {
        LayoutInflater inflater = LayoutInflater.from(this);
        ViewGroup parent = findViewById(R.id.main_frame);
        final View dialogView = inflater.inflate(R.layout.book_edit_dialog, parent, false);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MainActivity.this);
        alertDialogBuilder.setView(dialogView);

		final EditText titleInput = dialogView.findViewById(R.id.dialog_title_input);
		titleInput.setText(toEdit.getTitle());
		final EditText authorInput = dialogView.findViewById(R.id.dialog_author_input);
		authorInput.setText(toEdit.getAuthor());
        Button confirmEdit = dialogView.findViewById(R.id.book_edit_confirm_button);
        Button rejectEdit = dialogView.findViewById(R.id.book_edit_cancel_button);

        final AlertDialog dialogMade = alertDialogBuilder.create();
        if(dialogMade.getWindow() != null)
            dialogMade.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialogMade.show();

        confirmEdit.setOnClickListener(v -> {
            boolean correct = true;

			String title = titleInput.getText().toString().trim();
			String author = authorInput.getText().toString().trim();

			if (title.isEmpty()) {
				Snackbar snackBar = Snackbar.make(dialogView, getText(R.string.snack_bar_title), SNACK_BAR_DURATION);
				snackBar.show();
				correct = false;
			}

			if (correct && author.isEmpty()) {
				Snackbar snackBar = Snackbar.make(dialogView, getText(R.string.snack_bar_author), SNACK_BAR_DURATION);
				snackBar.show();
				correct = false;
			}

			if (correct) {
			    Book updated = new Book(title, author);
			    updated.setId(toEdit.getId());
				bookViewModel.updateRow(updated);
				dialogMade.dismiss();
			}
        });

        rejectEdit.setOnClickListener(v -> dialogMade.cancel());
    }
}
