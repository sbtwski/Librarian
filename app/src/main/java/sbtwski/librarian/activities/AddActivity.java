package sbtwski.librarian.activities;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.annimon.stream.Stream;

import org.joda.time.DateTime;
import org.joda.time.DateTimeComparator;

import java.util.List;

import androidx.lifecycle.ViewModelProviders;
import butterknife.BindView;
import butterknife.ButterKnife;
import sbtwski.librarian.R;
import sbtwski.librarian.activities.base.BaseToolbarActivity;
import sbtwski.librarian.entities.Book;
import sbtwski.librarian.entities.BorrowPeriod;
import sbtwski.librarian.entities.Borrower;
import sbtwski.librarian.entities.Borrowing;
import sbtwski.librarian.fragments.DatePickerFragment;
import sbtwski.librarian.utils.TimeFormattingUtils;
import sbtwski.librarian.viewmodels.BookViewModel;
import sbtwski.librarian.viewmodels.BorrowerViewModel;
import sbtwski.librarian.viewmodels.BorrowingViewModel;

import static com.annimon.stream.Collectors.toList;
import static sbtwski.librarian.utils.Constants.buttonClick;

public class AddActivity extends BaseToolbarActivity implements DatePickerDialog.OnDateSetListener {

	@BindView(R.id.book_title_input) EditText titleInput;
	@BindView(R.id.book_author_input) EditText authorInput;
	@BindView(R.id.borrower_name_input) EditText nameInput;
	@BindView(R.id.borrower_surname_input) EditText surnameInput;
	@BindView(R.id.borrow_date_input) TextView dateInput;
	@BindView(R.id.book_picker) Button bookPicker;
	@BindView(R.id.borrower_picker) Button borrowerPicker;

	private BookViewModel bookViewModel;
	private BorrowerViewModel borrowerViewModel;
	private BorrowingViewModel borrowingViewModel;

	private DateTime borrowDate;
	private List<Book> books;
	private List<Borrower> borrowers;
	private Long bookID, borrowerID;
	private boolean added = false;
	private boolean inserted = false;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		inflateContent(R.layout.activity_add);
        ButterKnife.bind(this);
		setupCustomButton(R.drawable.ic_check, new AddConfirmationListener());

		setupViewModels();
		getData();
		registerListeners();
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
	}

	@Override
	public void onDateSet(DatePicker view, int year, int month, int day) {
		DateTime dateTime = new DateTime(year, month+1, day, 0, 0);
		setDate(dateTime);
	}

	private void setupViewModels() {
		bookViewModel = ViewModelProviders.of(this).get(BookViewModel.class);
		borrowerViewModel = ViewModelProviders.of(this).get(BorrowerViewModel.class);
		borrowingViewModel = ViewModelProviders.of(this).get(BorrowingViewModel.class);
	}

	private void getData() {
		bookViewModel.getAllBooks().observe(this, (@NonNull final List<Book> database) ->{
		    books = database;
            if (!isNullOrEmpty(books)) {
				bookPicker.setVisibility(View.VISIBLE);
            }
        });
		borrowerViewModel.getAllBorrowers().observe(this, (@NonNull final List<Borrower> database) ->{
		    borrowers = database;
            if (!isNullOrEmpty(borrowers)) {
                borrowerPicker.setVisibility(View.VISIBLE);
            }
        });
		setupButtons();
	}

	private void setupButtons() {
		if (isNullOrEmpty(books)) {
			bookPicker.setVisibility(View.INVISIBLE);
		}

		if (isNullOrEmpty(borrowers)) {
			borrowerPicker.setVisibility(View.INVISIBLE);
		}
	}

	private void registerListeners() {
		bookPicker.setOnClickListener(view -> {
		    view.startAnimation(buttonClick);
			if (bookPicker.getVisibility() != View.VISIBLE) {
				return;
			}

			new AlertDialog.Builder(view.getContext())
					.setTitle(getString(R.string.book_picker_title))
					.setItems(
							prepareTitles().toArray(new String[0]),
							(dialog, book) -> {
								Book selected = books.get(book);
								titleInput.setText(selected.getTitle());
								authorInput.setText(selected.getAuthor());
							})
					.create()
					.show();
		});

		borrowerPicker.setOnClickListener(view -> {
		    view.startAnimation(buttonClick);
			if (borrowerPicker.getVisibility() != View.VISIBLE) {
				return;
			}

			new AlertDialog.Builder(view.getContext())
					.setTitle(getString(R.string.borrower_picker_title))
					.setItems(
							prepareBorrowersIds().toArray(new String[0]),
							(dialog, borrower) -> {
								Borrower selected = borrowers.get(borrower);
								nameInput.setText(selected.getName());
								surnameInput.setText(selected.getSurname());
							})
					.create()
					.show();
		});
	}

	public void showDataPickerFragment(View view) {
		new DatePickerFragment().show(getSupportFragmentManager(), "add_pick");
	}

	private void setDate(DateTime dateTime) {
		borrowDate = dateTime;
		String toPrint = TimeFormattingUtils.format(dateTime);
		((TextView) findViewById(R.id.borrow_date_input)).setText(toPrint);
	}

	private boolean isNullOrEmpty(List<?> list) {
		return list == null || list.size() == 0;
	}

	private List<String> prepareTitles() {
		return Stream.of(books)
				.map(Book::getTitle)
				.collect(toList());
	}

	private List<String> prepareBorrowersIds() {
		return Stream.of(borrowers)
				.map(Borrower::toString)
				.collect(toList());
	}

	private class AddConfirmationListener implements Toolbar.OnClickListener {

		@Override
		public void onClick(View v) {
			String title = trim(titleInput);
			String author = trim(authorInput);
			String name = trim(nameInput);
			String surname = trim(surnameInput);

			if (isInputValid(title, author, name, surname)) {
				Book borrowed = new Book(title, author);
				Borrower borrower = new Borrower(name, surname);
				finalCheck(borrowed, borrower);
			} else {
				setErrors(title, author, name, surname);
			}
		}

		private void setErrors(String title, String author, String name, String surname) {
			if (title.isEmpty()) {
				titleInput.setError(getString(R.string.empty_field));
			}

			if (author.isEmpty()) {
				authorInput.setError(getString(R.string.empty_field));
			}

			if (name.isEmpty()) {
				nameInput.setError(getString(R.string.empty_field));
			}

			if (surname.isEmpty()) {
				surnameInput.setError(getString(R.string.empty_field));
			}

			if (borrowDate == null) {
				dateInput.setError(getString(R.string.empty_field));
			}
		}

		private boolean isInputValid(String title, String author, String name, String surname) {
			return !title.isEmpty() && !author.isEmpty() && !name.isEmpty() && !surname.isEmpty() && borrowDate != null;
		}

		private String trim(EditText field) {
			return field.getText().toString().trim();
		}
	}

	private void finalCheck(Book toCheck, Borrower borrower){
		borrowingViewModel.getBookBorrowPeriods(toCheck).observe(this, (@Nullable List<BorrowPeriod> periods) -> {
			if(periods != null && !added) {
				if (noOverlappingPeriods(periods)) {
					added = true;
					Intent backToMain = new Intent(getApplicationContext(), MainActivity.class);
					insertIntoDatabase(toCheck, borrower);
					backToMain.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(backToMain);
				} else
					Toast.makeText(getApplicationContext(), getText(R.string.book_already_borrowed), Toast.LENGTH_LONG).show();
			}
		});
	}

	private boolean noOverlappingPeriods(List<BorrowPeriod> toCheck){
		boolean noOverlapping = true;
        DateTimeComparator comparator = DateTimeComparator.getDateOnlyInstance();

		for(BorrowPeriod current: toCheck){
			if(current.getReturnTime() == null){
				noOverlapping = false;
				break;
			}
			else{
				if(comparator.compare(borrowDate, current.getBorrowDateTime()) >= 0)
				    if(comparator.compare(borrowDate, current.getReturnDateTime()) < 0) {
                        noOverlapping = false;
                        break;
                    }
			}
		}

		return noOverlapping;
	}

	private void insertIntoDatabase(Book book, Borrower borrower){
		bookViewModel.insert(book);
		borrowerViewModel.insert(borrower);

        bookViewModel.getID(book).observe(this, (@Nullable final Long newBookID) -> {
            if(newBookID != null) {
                bookID = newBookID;
                addBorrowing();
            }
        });
        borrowerViewModel.getID(borrower).observe(this, (@Nullable final Long newBorrowerID) -> {
            if(newBorrowerID != null) {
                borrowerID = newBorrowerID;
                addBorrowing();
            }
        });
	}

	private void addBorrowing(){
        if(bookID != null && borrowerID != null && !inserted) {
            inserted = true;
            Borrowing borrowing = new Borrowing(bookID, borrowerID, borrowDate.getMillis(), null);
            borrowingViewModel.insert(borrowing);
        }
    }
}
