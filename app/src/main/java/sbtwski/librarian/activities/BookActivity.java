package sbtwski.librarian.activities;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;

import android.view.View;
import android.widget.TextView;

import java.util.List;

import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import sbtwski.librarian.R;
import sbtwski.librarian.activities.base.BaseToolbarActivity;
import sbtwski.librarian.adapters.BookPreviewAdapter;
import sbtwski.librarian.entities.Book;
import sbtwski.librarian.entities.BookBorrower;
import sbtwski.librarian.viewmodels.BorrowingViewModel;

public class BookActivity extends BaseToolbarActivity {

	@BindView(R.id.book_author_book_preview) TextView author;
	@BindView(R.id.book_title_book_preview) TextView title;

	private Book book;
	private BookPreviewAdapter adapter;
	private BorrowingViewModel borrowingViewModel;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		inflateContent(R.layout.activity_book);
        ButterKnife.bind(this);

		book = (Book) getIntent().getSerializableExtra("book");
		getData();

		setupCustomButton(R.drawable.ic_back, new BookActivityFinishListener());
		adapter = new BookPreviewAdapter();

		RecyclerView borrowersPreview = findViewById(R.id.book_preview_recycler);
		borrowersPreview.setLayoutManager(new LinearLayoutManager(this));
		borrowersPreview.setAdapter(adapter);

		fillDataFields();
	}

	private void fillDataFields() {
		author.setText(book.getAuthor());
		title.setText(book.getTitle());
	}

	private void getData(){
	    borrowingViewModel = ViewModelProviders.of(this).get(BorrowingViewModel.class);
        borrowingViewModel.getBookBorrowers(book).observe(this, (@Nullable final List<BookBorrower> borrowers) -> adapter.setDatabase(borrowers));
	}

    private class BookActivityFinishListener implements Toolbar.OnClickListener {
        @Override
        public void onClick(View v) {
            removeObserver();

            Intent toMain = new Intent(getApplicationContext(), MainActivity.class);
            toMain.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            toMain.putExtra("bookTAB",true);
            startActivity(toMain);
        }
    }

    private void removeObserver(){
        borrowingViewModel.getBookBorrowers(book).removeObservers(this);
    }
}
