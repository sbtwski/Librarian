package sbtwski.librarian.activities;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.Nullable;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import butterknife.BindView;
import butterknife.ButterKnife;
import sbtwski.librarian.R;
import sbtwski.librarian.activities.base.SimpleToolbarActivity;

import static sbtwski.librarian.utils.Constants.*;

public class SettingsActivity extends SimpleToolbarActivity {

    @BindView(R.id.books_sort_button) Button booksSort;
    @BindView(R.id.swipe_actions_button) Button swipeActions;
    @BindView(R.id.borrowers_sort_button) Button borrowersSort;
    @BindView(R.id.long_press_checkbox) CheckBox longPressBox;

	private boolean editOnLeft, longPressEnabled, sortTypeChanged = false;
	private int bookSortTypeId, borrowerSortTypeId, temporarySortId;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		inflateContent(R.layout.activity_settings);
        ButterKnife.bind(this);
        setupCustomButton(new SettingsReturnListener());

        readPreferences();
        longPressBox.setChecked(longPressEnabled);
        addListener();
	}

    @Override
    protected void onStop() {
        super.onStop();
        savePreferences();
    }

    private void addListener() {
		swipeActions.setOnClickListener((View v) -> buildSwipeActionsDialog());
		booksSort.setOnClickListener((View v) -> buildBooksSortDialog());
		borrowersSort.setOnClickListener((View v) -> buildBorrowersSortDialog());
		longPressBox.setOnCheckedChangeListener((CompoundButton buttonView, boolean isChecked) -> longPressEnabled = isChecked);
	}

	private void buildSwipeActionsDialog() {
        View viewInflated = genericDialogViewInflater(R.layout.swipe_actions_dialog);

        Switch invertSwitch = viewInflated.findViewById(R.id.invert_switch);
        final FrameLayout leftFrame = viewInflated.findViewById(R.id.left_preview_frame);
        final FrameLayout rightFrame = viewInflated.findViewById(R.id.right_preview_frame);
        final TextView leftIcon = viewInflated.findViewById(R.id.left_preview_icon);
        final TextView rightIcon = viewInflated.findViewById(R.id.right_preview_icon);

        updatePreview(leftFrame,rightFrame,leftIcon,rightIcon, editOnLeft);

        invertSwitch.setChecked(!editOnLeft);
        invertSwitch.setOnCheckedChangeListener((CompoundButton buttonView, boolean isChecked) ->
                updatePreview(leftFrame,rightFrame,leftIcon,rightIcon, !isChecked));

        AlertDialog dialogBuilt = genericDialogBuilder(R.string.swipe_settings_button_text, viewInflated);
        dialogBuilt.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener((View v) -> {
            editOnLeft = !invertSwitch.isChecked();
            dialogBuilt.dismiss();
        });
	}

	private void buildBooksSortDialog() {
        View viewInflated = genericDialogViewInflater(R.layout.books_sort_dialog);

        RadioGroup bookSortTypes = viewInflated.findViewById(R.id.books_sort_types);
        if(bookSortTypeId > 0){
            Integer idFromMap = BOOK_RADIO_BUTTONS_MAP.inverse().get(bookSortTypeId);
            if(idFromMap != null)
                bookSortTypes.check(idFromMap);
        }

        AlertDialog dialogBuilt = genericDialogBuilder(R.string.books_sort_button_text, viewInflated);
		dialogBuilt.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener((View v) -> {
            if(sortTypeChanged) {
                sortTypeChanged = false;
                Integer fromMap = BOOK_RADIO_BUTTONS_MAP.get(temporarySortId);
                bookSortTypeId = fromMap != null ? fromMap : -1;
            }
            dialogBuilt.dismiss();
		});
	}

	private void buildBorrowersSortDialog() {
		View viewInflated = genericDialogViewInflater(R.layout.borrowers_sort_dialog);

        RadioGroup borrowerSortTypes = viewInflated.findViewById(R.id.borrowers_sort_types);
        if(borrowerSortTypeId > 0){
            Integer idFromMap = BORROWER_RADIO_BUTTONS_MAP.inverse().get(borrowerSortTypeId);
            if(idFromMap != null)
                borrowerSortTypes.check(idFromMap);
        }

		AlertDialog dialogBuilt = genericDialogBuilder(R.string.borrowers_sort_button_text, viewInflated);
		dialogBuilt.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener((View v) -> {
            if(sortTypeChanged) {
                sortTypeChanged = false;
                Integer fromMap = BORROWER_RADIO_BUTTONS_MAP.get(temporarySortId);
                borrowerSortTypeId = fromMap != null ? fromMap : -1;
            }
            dialogBuilt.dismiss();
		});
    }

	private View genericDialogViewInflater(int layoutID){
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ViewGroup parent = findViewById(android.R.id.content);
        return inflater.inflate(layoutID, parent, false);
    }

	private AlertDialog genericDialogBuilder(int titleID, View inflatedView){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(SettingsActivity.this);
        alertDialogBuilder
                .setTitle(getString(titleID))
                .setView(inflatedView)
                .setPositiveButton(R.string.accept_dialog, null)
                .setNegativeButton(R.string.reject_dialog, null);

        AlertDialog dialogMade = alertDialogBuilder.create();
        dialogMade.show();
        dialogMade.getButton(AlertDialog.BUTTON_NEGATIVE).setOnClickListener((View v) -> dialogMade.dismiss());

        return dialogMade;
    }

	private void updatePreview(FrameLayout leftFrame, FrameLayout rightFrame, TextView leftIcon, TextView rightIcon, boolean onLeft) {
		if(onLeft) {
			leftFrame.setBackgroundColor(getResources().getColor(R.color.colorAdditional_v1));
			rightFrame.setBackgroundColor(getResources().getColor(R.color.colorAdditional_v2));
			leftIcon.setBackground(getDrawable(R.drawable.ic_edit));
			rightIcon.setBackground(getDrawable(R.drawable.ic_delete));
		}
		else {
			leftFrame.setBackgroundColor(getResources().getColor(R.color.colorAdditional_v2));
			rightFrame.setBackgroundColor(getResources().getColor(R.color.colorAdditional_v1));
			leftIcon.setBackground(getDrawable(R.drawable.ic_delete));
			rightIcon.setBackground(getDrawable(R.drawable.ic_edit));
		}
	}

	public void bookSortTypeChosen(View view) {
		boolean checked = ((RadioButton) view).isChecked();

		if(checked) {
			if(view.getId() != bookSortTypeId) {
				temporarySortId = view.getId();
				sortTypeChanged = true;
			}
			else
				sortTypeChanged = false;
		}
	}

	public void borrowerSortTypeChosen(View view) {
		boolean checked = ((RadioButton) view).isChecked();

		if(checked) {
			if(view.getId() != borrowerSortTypeId) {
				temporarySortId = view.getId();
				sortTypeChanged = true;
			}
			else
				sortTypeChanged = false;
		}
	}

    private void readPreferences(){
        SharedPreferences sharedPref = getSharedPreferences(SP_NAME, Context.MODE_PRIVATE);

        editOnLeft = sharedPref.getBoolean(SWIPE_CODE, true);
        longPressEnabled = sharedPref.getBoolean(LONG_PRESS_CODE, false);
        bookSortTypeId = sharedPref.getInt(BOOK_SORT_CODE, -1);
        borrowerSortTypeId = sharedPref.getInt(BORROWER_SORT_CODE, -1);
    }

    private void savePreferences(){
        SharedPreferences sharedPref = getSharedPreferences(SP_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();

        editor.putBoolean(LONG_PRESS_CODE, longPressEnabled);
        editor.putBoolean(SWIPE_CODE, editOnLeft);
        editor.putInt(BOOK_SORT_CODE, bookSortTypeId);
        editor.putInt(BORROWER_SORT_CODE, borrowerSortTypeId);
        editor.apply();
    }

    private class SettingsReturnListener implements Toolbar.OnClickListener {
        @Override
        public void onClick(View v) {
            savePreferences();
            finish();
        }
    }
}
