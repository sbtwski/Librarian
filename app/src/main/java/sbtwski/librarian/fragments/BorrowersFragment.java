package sbtwski.librarian.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.Collections;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import sbtwski.librarian.R;
import sbtwski.librarian.adapters.BorrowersAdapter;
import sbtwski.librarian.entities.BookBorrower;
import sbtwski.librarian.utils.Constants;
import sbtwski.librarian.handlers.ClickHandler;
import sbtwski.librarian.handlers.MainClickHandler;
import sbtwski.librarian.swipe.SwipeController;
import sbtwski.librarian.swipe.SwipeControllerActions;
import sbtwski.librarian.viewmodels.BorrowerViewModel;
import sbtwski.librarian.viewmodels.BorrowingViewModel;

import static sbtwski.librarian.utils.Constants.*;
import static sbtwski.librarian.utils.BorrowerComparators.COMPARATORS_MAP;

public class BorrowersFragment extends Fragment {
    private ClickHandler borrowersHandler;
    private BorrowersAdapter adapter;
    private SwipeController borrowerSwipe = null;
    private BorrowerViewModel borrowerViewModel;
    private boolean longPressEnabled, editOnLeft;
    private int borrowersOrder;

    @Override
    @SuppressWarnings("unchecked")
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        BorrowingViewModel borrowingViewModel;

        if(getActivity() != null)
            borrowingViewModel = ViewModelProviders.of(getActivity()).get(BorrowingViewModel.class);
        else
            borrowingViewModel = ViewModelProviders.of(this).get(BorrowingViewModel.class);

        borrowingViewModel.getAllBorrowers().observe(this, this::setOrderedDatabase);

        borrowerViewModel = ViewModelProviders.of(this).get(BorrowerViewModel.class);

        configureHandler();
        return inflater.inflate(R.layout.borrowers_layout, container, false);
    }

    @Override
    public void onResume() {
        super.onResume();
        readPreferences();
        setOrderedDatabase(adapter.getDatabase());
    }

    @Override
    @SuppressWarnings("unchecked")
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if(getActivity() != null) {
            adapter = new BorrowersAdapter(borrowersHandler);

            RecyclerView borrowersView = getActivity().findViewById(R.id.borrowers_recycler_view);
            borrowersView.setLayoutManager(new LinearLayoutManager(getContext()));
            borrowersView.setAdapter(adapter);

            DisplayMetrics displayMetrics = getActivity().getResources().getDisplayMetrics();
            float width = displayMetrics.widthPixels;

            borrowerSwipe = new SwipeController(new SwipeControllerActions() {
                @Override
                public void onRightClicked(int position) {
                    if(editOnLeft)
                        triggerDelete(position);
                    else
                        triggerEdit(position);
                }

                @Override
                public void onLeftClicked(int position) {
                    if(editOnLeft)
                        triggerEdit(position);
                    else
                        triggerDelete(position);
                }
            }, editOnLeft, width);

            ItemTouchHelper itemTouchHelper = new ItemTouchHelper(borrowerSwipe);
            itemTouchHelper.attachToRecyclerView(borrowersView);

            borrowersView.addItemDecoration(new RecyclerView.ItemDecoration() {
                @Override
                public void onDraw(@NonNull Canvas c, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
                    borrowerSwipe.onDraw(c);
                }
            });
        }
    }

    private void triggerEdit(int position){
        if(getActivity() != null) {
            BookBorrower toEdit = adapter.getItem(position);
            ((MainClickHandler) getActivity()).borrowerEdit(toEdit);
        }
    }

    private void triggerDelete(int position){
        if(getActivity() != null) {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
            LayoutInflater inflater = this.getLayoutInflater();
            ViewGroup parent = getActivity().findViewById(R.id.main_frame);
            View dialogView = inflater.inflate(R.layout.delete_confirm_dialog, parent,false);
            dialogBuilder.setView(dialogView);

            final AlertDialog dialogMade = dialogBuilder.create();
            if(dialogMade.getWindow() != null)
                dialogMade.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialogMade.show();

            Button confirmDelete = dialogView.findViewById(R.id.delete_confirm_button);
            Button rejectDelete = dialogView.findViewById(R.id.delete_cancel_button);

            confirmDelete.setOnClickListener(v -> {
                borrowerViewModel.deleteRow(adapter.getItem(position).getId());
                dialogMade.dismiss();
            });

            rejectDelete.setOnClickListener(v -> dialogMade.dismiss());
        }
    }

    private void configureHandler() {
        borrowersHandler = new ClickHandler() {
            @Override
            public void onItemClicked(int position) {
                if(getActivity() != null) {
                    BookBorrower clicked = adapter.getItem(position);
                    ((MainClickHandler) getActivity()).borrowerClicked(clicked.getId());
                }
            }

            @Override
            public void onItemLongClicked(int position) {
                if(longPressEnabled && getActivity() != null){
                    BookBorrower clicked = adapter.getItem(position);
                    ((MainClickHandler) getActivity()).borrowerEdit(clicked);
                }
            }

            @Override
            public void onReturnClicked(int position) {

            }
        };
    }

    private void readPreferences(){
        if(getActivity() != null) {
            SharedPreferences sharedPref = getActivity().getSharedPreferences(Constants.SP_NAME, Context.MODE_PRIVATE);

            editOnLeft = sharedPref.getBoolean(SWIPE_CODE, true);
            borrowerSwipe.updateSettings(editOnLeft);
            longPressEnabled = sharedPref.getBoolean(LONG_PRESS_CODE, false);
            borrowersOrder = sharedPref.getInt(BORROWER_SORT_CODE, -1);
        }
    }

    private void setOrderedDatabase(List<BookBorrower> data){
        if(borrowersOrder > 0)
            Collections.sort(data, COMPARATORS_MAP.get(borrowersOrder));

        adapter.setDatabase(data);
    }
}
