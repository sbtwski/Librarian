package sbtwski.librarian.fragments;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentActivity;

import com.annimon.stream.Optional;

import org.joda.time.DateTime;

public class DatePickerFragment extends DialogFragment {

	private Long calendarLimit;

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Bundle data = getArguments();
		if (data != null) {
			calendarLimit = data.getLong("limit");
		}
	}

	@Override
	@NonNull
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		DateTime dateTime = new DateTime();

		DatePickerDialog dialog = new DatePickerDialog(
				activity(),
				(DatePickerDialog.OnDateSetListener) activity(),
				dateTime.getYear(),
				dateTime.getMonthOfYear(),
				dateTime.getDayOfMonth()
		);

		if (calendarLimit != null) {
			dialog.getDatePicker().setMinDate(calendarLimit);
		}

		dialog.getDatePicker().setMaxDate(dateTime.getMillis());

		return dialog;
	}

	private FragmentActivity activity() {
		return Optional.ofNullable(getActivity())
				.orElseThrow(() -> new IllegalStateException("Activity is null."));
	}
}
