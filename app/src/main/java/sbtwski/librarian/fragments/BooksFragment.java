package sbtwski.librarian.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.Collections;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import sbtwski.librarian.R;
import sbtwski.librarian.adapters.BooksAdapter;
import sbtwski.librarian.entities.BorrowedBook;
import sbtwski.librarian.utils.Constants;
import sbtwski.librarian.handlers.ClickHandler;
import sbtwski.librarian.handlers.MainClickHandler;
import sbtwski.librarian.swipe.SwipeController;
import sbtwski.librarian.swipe.SwipeControllerActions;
import sbtwski.librarian.viewmodels.BookViewModel;
import sbtwski.librarian.viewmodels.BorrowingViewModel;

import static sbtwski.librarian.utils.Constants.*;
import static sbtwski.librarian.utils.BookComparators.COMPARATORS_MAP;

public class BooksFragment extends Fragment {
    private ClickHandler booksHandler;
    private BooksAdapter adapter;
    private SwipeController bookSwipe;
    private BookViewModel bookViewModel;
    private boolean longPressEnabled, editOnLeft;
    private int booksOrder;

    @Override
    @SuppressWarnings("unchecked")
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        BorrowingViewModel borrowingViewModel;

        if(getActivity() != null)
            borrowingViewModel = ViewModelProviders.of(getActivity()).get(BorrowingViewModel.class);
        else
            borrowingViewModel = ViewModelProviders.of(this).get(BorrowingViewModel.class);

        borrowingViewModel.getAllBooks().observe(this, this::setOrderedDatabase);

        bookViewModel = ViewModelProviders.of(this).get(BookViewModel.class);

        configureHandler();
        return inflater.inflate(R.layout.books_layout, container, false);
    }

    @Override
    public void onResume() {
        super.onResume();
        readPreferences();
        setOrderedDatabase(adapter.getDatabase());
    }

    @Override
    @SuppressWarnings("unchecked")
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if(getActivity() != null) {
            adapter = new BooksAdapter(booksHandler);

            RecyclerView booksView = getActivity().findViewById(R.id.books_recycler_view);
            booksView.setLayoutManager(new LinearLayoutManager(getContext()));
            booksView.setAdapter(adapter);

            DisplayMetrics displayMetrics = getActivity().getResources().getDisplayMetrics();
            float width = displayMetrics.widthPixels;

            bookSwipe = new SwipeController(new SwipeControllerActions() {
                @Override
                public void onRightClicked(int position) {
                    if(editOnLeft)
                        triggerDelete(position);
                    else
                        triggerEdit(position);
                }

                @Override
                public void onLeftClicked(int position) {
                    if(editOnLeft)
                        triggerEdit(position);
                    else
                        triggerDelete(position);
                }
            }, editOnLeft, width);
            ItemTouchHelper itemTouchHelper = new ItemTouchHelper(bookSwipe);
            itemTouchHelper.attachToRecyclerView(booksView);

            booksView.addItemDecoration(new RecyclerView.ItemDecoration() {
                @Override
                public void onDraw(@NonNull Canvas c, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
                    bookSwipe.onDraw(c);
                }
            });
        }
    }

    private void triggerEdit(int position){
        if(getActivity() != null) {
            BorrowedBook toEdit = adapter.getItem(position);
            ((MainClickHandler) getActivity()).bookEdit(toEdit);
        }
    }

    private void triggerDelete(int position){
        if(getActivity() != null) {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
            LayoutInflater inflater = this.getLayoutInflater();
            ViewGroup parent = getActivity().findViewById(R.id.main_frame);
            View dialogView = inflater.inflate(R.layout.delete_confirm_dialog, parent,false);
            dialogBuilder.setView(dialogView);

            Button confirmDelete = dialogView.findViewById(R.id.delete_confirm_button);
            Button rejectDelete = dialogView.findViewById(R.id.delete_cancel_button);

            final AlertDialog dialogMade = dialogBuilder.create();
            if(dialogMade.getWindow() != null)
                dialogMade.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialogMade.show();

            confirmDelete.setOnClickListener(v -> {
                bookViewModel.deleteRow(adapter.getItem(position).getId());
                dialogMade.dismiss();
            });

            rejectDelete.setOnClickListener(v -> dialogMade.dismiss());
        }
    }

    private void configureHandler() {
        booksHandler = new ClickHandler() {
            @Override
            public void onItemClicked(int position) {
                if(getActivity() != null) {
                    BorrowedBook clicked = adapter.getItem(position);
                    ((MainClickHandler) getActivity()).bookClicked(clicked.getId());
                }
            }

            @Override
            public void onItemLongClicked(int position) {
                if(longPressEnabled && getActivity() != null) {
                    BorrowedBook clicked = adapter.getItem(position);
                    ((MainClickHandler) getActivity()).bookEdit(clicked);
                }
            }

            @Override
            public void onReturnClicked(int position) {
                if(getActivity() != null) {
                    BorrowedBook clicked = adapter.getItem(position);
                    ((MainClickHandler) getActivity()).bookReturn(clicked);
                }
            }
        };
    }

    private void readPreferences(){
        if(getActivity() != null) {
            SharedPreferences sharedPref = getActivity().getSharedPreferences(Constants.SP_NAME, Context.MODE_PRIVATE);

            editOnLeft = sharedPref.getBoolean(SWIPE_CODE, true);
            bookSwipe.updateSettings(editOnLeft);
            longPressEnabled = sharedPref.getBoolean(LONG_PRESS_CODE, false);
            booksOrder = sharedPref.getInt(BOOK_SORT_CODE, -1);
        }
    }

    private void setOrderedDatabase(List<BorrowedBook> data){
        if(booksOrder > 0)
            Collections.sort(data, COMPARATORS_MAP.get(booksOrder));

        adapter.setDatabase(data);
    }
}

