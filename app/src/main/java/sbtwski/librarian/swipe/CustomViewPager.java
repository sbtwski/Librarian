package sbtwski.librarian.swipe;

import android.content.Context;
import androidx.viewpager.widget.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

public class CustomViewPager extends ViewPager {

	private boolean swipeEnabled = true;

	public CustomViewPager(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		return this.swipeEnabled && super.onTouchEvent(event) && performClick();
	}

	@Override
	public boolean performClick() {
		return super.performClick();
	}

	@Override
	public boolean onInterceptTouchEvent(MotionEvent event) {
		return this.swipeEnabled && super.onInterceptTouchEvent(event);
	}

	public void setSwipingEnabled(boolean enabled) {
		this.swipeEnabled = enabled;
	}
}
