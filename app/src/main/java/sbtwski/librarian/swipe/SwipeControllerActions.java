package sbtwski.librarian.swipe;

public interface SwipeControllerActions {
	void onLeftClicked(int position);
	void onRightClicked(int position);
}
