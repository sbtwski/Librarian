package sbtwski.librarian.swipe;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.MotionEvent;
import android.view.View;

import sbtwski.librarian.R;

import static androidx.recyclerview.widget.ItemTouchHelper.*;

public class SwipeController extends Callback {
	private boolean swipeBack = false;
	private ButtonsState buttonShowedState = ButtonsState.GONE;
	private RectF buttonInstance = null;
	private RecyclerView.ViewHolder currentItemViewHolder = null;
	private SwipeControllerActions buttonsActions;
	private float buttonWidth;
	private boolean editOnLeft;

	public SwipeController(SwipeControllerActions buttonsActions, boolean editOnLeft, float screenWidth) {
		this.buttonsActions = buttonsActions;
		this.editOnLeft = editOnLeft;
		buttonWidth = screenWidth/3.5f;
	}

	@Override
	public int getMovementFlags(@NonNull RecyclerView recyclerView,@NonNull RecyclerView.ViewHolder viewHolder) {
		return makeMovementFlags(0, LEFT | RIGHT);
	}

	@Override
	public boolean onMove(@NonNull RecyclerView recyclerView,@NonNull RecyclerView.ViewHolder viewHolder,@NonNull RecyclerView.ViewHolder target) {
		return false;
	}

	@Override
	public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {

	}

	@Override
	public int convertToAbsoluteDirection(int flags, int layoutDirection) {
		if (swipeBack) {
			swipeBack = buttonShowedState != ButtonsState.GONE;
			return 0;
		}
		return super.convertToAbsoluteDirection(flags, layoutDirection);
	}

	@Override
	public void onChildDraw(@NonNull Canvas c,@NonNull RecyclerView recyclerView,@NonNull RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
		if (actionState == ACTION_STATE_SWIPE) {
			if (buttonShowedState != ButtonsState.GONE) {
				if (buttonShowedState == ButtonsState.LEFT_VISIBLE) dX = Math.max(dX, buttonWidth);
				if (buttonShowedState == ButtonsState.RIGHT_VISIBLE)
					dX = Math.min(dX, -buttonWidth);
				super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
			} else {
				setTouchListener(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
			}
		}

		if (buttonShowedState == ButtonsState.GONE) {
			super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
		}
		currentItemViewHolder = viewHolder;
	}

	@SuppressLint("ClickableViewAccessibility")
	private void setTouchListener(final Canvas c, final RecyclerView recyclerView, final RecyclerView.ViewHolder viewHolder, final float dX, final float dY, final int actionState, final boolean isCurrentlyActive) {
		recyclerView.setOnTouchListener((v, event) -> {
			swipeBack = event.getAction() == MotionEvent.ACTION_CANCEL || event.getAction() == MotionEvent.ACTION_UP;
			if (swipeBack) {
				if (dX < -buttonWidth) buttonShowedState = ButtonsState.RIGHT_VISIBLE;
				else if (dX > buttonWidth) buttonShowedState = ButtonsState.LEFT_VISIBLE;

				if (buttonShowedState != ButtonsState.GONE) {
					setTouchDownListener(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
					setItemsClickable(recyclerView, false);
				}
			}
			return false;
		});
	}

	@SuppressLint("ClickableViewAccessibility")
	private void setTouchDownListener(final Canvas c, final RecyclerView recyclerView, final RecyclerView.ViewHolder viewHolder, final float dX, final float dY, final int actionState, final boolean isCurrentlyActive) {
		recyclerView.setOnTouchListener((v, event) -> {
			if (event.getAction() == MotionEvent.ACTION_DOWN) {
				setTouchUpListener(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
			}
			return false;
		});
	}

	@SuppressLint("ClickableViewAccessibility")
	private void setTouchUpListener(final Canvas c, final RecyclerView recyclerView, final RecyclerView.ViewHolder viewHolder, final float dX, final float dY, final int actionState, final boolean isCurrentlyActive) {
		recyclerView.setOnTouchListener((v, event) -> {
			if (event.getAction() == MotionEvent.ACTION_UP) {
				SwipeController.super.onChildDraw(c, recyclerView, viewHolder, 0F, dY, actionState, isCurrentlyActive);
				recyclerView.setOnTouchListener((v1, event1) -> false);
				setItemsClickable(recyclerView, true);
				swipeBack = false;

				if (buttonsActions != null && buttonInstance != null && buttonInstance.contains(event.getX(), event.getY())) {
					if (buttonShowedState == ButtonsState.LEFT_VISIBLE) {
						buttonsActions.onLeftClicked(viewHolder.getAdapterPosition());
					} else if (buttonShowedState == ButtonsState.RIGHT_VISIBLE) {
						buttonsActions.onRightClicked(viewHolder.getAdapterPosition());
					}
				}
				buttonShowedState = ButtonsState.GONE;
				currentItemViewHolder = null;
			}
			return false;
		});
	}

	private void setItemsClickable(RecyclerView recyclerView, boolean isClickable) {
		for (int i = 0; i < recyclerView.getChildCount(); ++i) {
			recyclerView.getChildAt(i).setClickable(isClickable);
		}
	}

	private void drawButtons(Canvas canvas, RecyclerView.ViewHolder viewHolder) {
		float buttonWidthWithoutPadding = buttonWidth - 20;

		View itemView = viewHolder.itemView;
		Paint foregroundPaint = new Paint();
		Paint backgroundPaint = new Paint();

		foregroundPaint.setColor(itemView.getResources().getColor(R.color.colorBackground));

		RectF leftButton = new RectF(itemView.getLeft(), itemView.getTop(), itemView.getLeft() + buttonWidthWithoutPadding, itemView.getBottom());
		RectF rightButton = new RectF(itemView.getRight() - buttonWidthWithoutPadding, itemView.getTop(), itemView.getRight(), itemView.getBottom());

		Bitmap icon;

		if(editOnLeft) {
			backgroundPaint.setColor(itemView.getResources().getColor(R.color.colorAdditional_v1));
			icon = BitmapFactory.decodeResource(itemView.getResources(), R.drawable.ic_edit);
		}
		else {
			backgroundPaint.setColor(itemView.getResources().getColor(R.color.colorAdditional_v2));
			icon = BitmapFactory.decodeResource(itemView.getResources(), R.drawable.ic_delete);
		}

		canvas.drawRoundRect(leftButton, 25, 25, backgroundPaint);
		drawIcon(canvas, icon, leftButton, foregroundPaint);

		if(editOnLeft) {
			backgroundPaint.setColor(itemView.getResources().getColor(R.color.colorAdditional_v2));
			icon = BitmapFactory.decodeResource(itemView.getResources(), R.drawable.ic_delete);
		}
		else {
			backgroundPaint.setColor(itemView.getResources().getColor(R.color.colorAdditional_v1));
			icon = BitmapFactory.decodeResource(itemView.getResources(), R.drawable.ic_edit);
		}

		canvas.drawRoundRect(rightButton, 25, 25, backgroundPaint);
		drawIcon(canvas, icon, rightButton, foregroundPaint);

		buttonInstance = null;
		if (buttonShowedState == ButtonsState.LEFT_VISIBLE) {
			buttonInstance = leftButton;
		} else if (buttonShowedState == ButtonsState.RIGHT_VISIBLE) {
			buttonInstance = rightButton;
		}
	}

	public void onDraw(Canvas c) {
		if (currentItemViewHolder != null) {
			drawButtons(c, currentItemViewHolder);
		}
	}

	private void drawIcon(Canvas canvas, Bitmap toDraw, RectF button, Paint paint){
		int widthOffset = toDraw.getWidth() / 2;
		int heightOffset = toDraw.getHeight() / 2;

		canvas.drawBitmap(toDraw, button.centerX() - widthOffset, button.centerY() - heightOffset, paint);
	}

	public void updateSettings(boolean editOnLeft){
	    this.editOnLeft = editOnLeft;
    }
}
