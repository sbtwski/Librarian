package sbtwski.librarian.handlers;

public interface ClickHandler {
    void onItemClicked(int position);
    void onItemLongClicked(int position);
    void onReturnClicked(int position);
}