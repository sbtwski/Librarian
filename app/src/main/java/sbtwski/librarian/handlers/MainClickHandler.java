package sbtwski.librarian.handlers;

import sbtwski.librarian.entities.BookBorrower;
import sbtwski.librarian.entities.BorrowedBook;

public interface MainClickHandler {
    void borrowerClicked(Long borrowerID);
    void bookClicked(Long bookID);
    void bookReturn(BorrowedBook toReturn);

    void bookEdit(BorrowedBook toEdit);
    void borrowerEdit(BookBorrower toEdit);
}
