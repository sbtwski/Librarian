package sbtwski.librarian.adapters;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import sbtwski.librarian.fragments.BooksFragment;
import sbtwski.librarian.fragments.BorrowersFragment;

public class TabAdapter extends FragmentStatePagerAdapter {
    private int numOfTabs;

    public TabAdapter(FragmentManager manager, int numOfTabs) {
        super(manager);
        this.numOfTabs = numOfTabs;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new BorrowersFragment();
            case 1:
                return new BooksFragment();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return numOfTabs;
    }
}
