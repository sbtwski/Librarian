package sbtwski.librarian.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import sbtwski.librarian.R;
import sbtwski.librarian.entities.BookBorrower;
import sbtwski.librarian.handlers.ClickHandler;
import sbtwski.librarian.adapters.holders.BorrowerViewHolder;

public class BorrowersAdapter extends RecyclerView.Adapter<BorrowerViewHolder> {
    private List<BookBorrower> database;
    private ClickHandler adapterHandler;

    public BorrowersAdapter(final ClickHandler fragmentHandler) {
        database = new ArrayList<>();
        adapterHandler = fragmentHandler;
    }

    @Override
    public void onBindViewHolder(@NonNull BorrowerViewHolder borrowerHolder, int i) {
        BookBorrower toBind = database.get(i);
        borrowerHolder.bind(toBind, adapterHandler);
    }

    @Override
    @NonNull
    public BorrowerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.borrowers_list_item, parent, false);
        return new BorrowerViewHolder(view);
    }

    @Override
    public int getItemCount() {
        if(database != null)
            return database.size();
        else
            return 0;
    }

    public List<BookBorrower> getDatabase(){
        return database;
    }

    public void setDatabase(List<BookBorrower> database) {
        if(database != null) {
            this.database = database;
            notifyDataSetChanged();
        }
    }

    public BookBorrower getItem(int position){
        BookBorrower atPosition = null;
        if(position < database.size())
            atPosition =  database.get(position);
        return atPosition;
    }
}

