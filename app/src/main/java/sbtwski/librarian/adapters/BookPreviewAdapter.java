package sbtwski.librarian.adapters;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import sbtwski.librarian.R;
import sbtwski.librarian.adapters.holders.BorrowerViewHolderInPreview;
import sbtwski.librarian.entities.BookBorrower;

public class BookPreviewAdapter extends RecyclerView.Adapter<BorrowerViewHolderInPreview> {

	private List<BookBorrower> borrowings;

	public BookPreviewAdapter() {
		this.borrowings = new ArrayList<>();
	}

	@Override
	public void onBindViewHolder(@NonNull BorrowerViewHolderInPreview holder, int i) {
		holder.bind(borrowings.get(i));
	}

	@Override
	@NonNull
	public BorrowerViewHolderInPreview onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
		LayoutInflater inflater = LayoutInflater.from(parent.getContext());
		View view = inflater.inflate(R.layout.borrowers_list_item_book_preview, parent, false);
		return new BorrowerViewHolderInPreview(view);
	}

	@Override
	public int getItemCount() {
		return borrowings.size();
	}

	public void setDatabase(List<BookBorrower> database) {
		if(database != null) {
			this.borrowings = database;
			notifyDataSetChanged();
		}
	}
}
