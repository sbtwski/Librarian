package sbtwski.librarian.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import sbtwski.librarian.R;
import sbtwski.librarian.entities.BorrowedBook;
import sbtwski.librarian.handlers.ClickHandler;
import sbtwski.librarian.adapters.holders.BookViewHolder;

public class BooksAdapter extends RecyclerView.Adapter<BookViewHolder> {
    private List<BorrowedBook> database;
    private ClickHandler adapterHandler;

    public BooksAdapter(final ClickHandler fragmentHandler) {
        database = new ArrayList<>();
        adapterHandler = fragmentHandler;
    }

    @Override
    public void onBindViewHolder(@NonNull BookViewHolder bookHolder, int i) {
        BorrowedBook toBind = database.get(i);
        bookHolder.bind(toBind, adapterHandler);
    }

    @Override
    @NonNull
    public BookViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.books_list_item, parent, false);
        return new BookViewHolder(view);
    }

    @Override
    public int getItemCount() {
        if(database != null)
            return database.size();
        else
            return 0;
    }

    public List<BorrowedBook> getDatabase(){
        return database;
    }

    public void setDatabase(List<BorrowedBook> database) {
        if(database != null) {
            this.database = database;
            notifyDataSetChanged();
        }
    }

    public BorrowedBook getItem(int position){
        BorrowedBook atPosition = null;
        if(position < database.size())
            atPosition = database.get(position);
        return atPosition;
    }
}
