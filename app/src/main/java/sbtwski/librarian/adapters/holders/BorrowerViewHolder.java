package sbtwski.librarian.adapters.holders;

import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.view.View;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import sbtwski.librarian.R;
import sbtwski.librarian.entities.BookBorrower;
import sbtwski.librarian.handlers.ClickHandler;

public class BorrowerViewHolder extends RecyclerView.ViewHolder {

	@BindView(R.id.borrower_personal_data) TextView personalData;
	@BindView(R.id.borrowed_books_number) TextView numberOfBooks;
	@BindView(R.id.borrowed_books_description) TextView numberDescription;
	@BindView(R.id.borrowed_books_container) View booksNumberContainer;

	private boolean numberOfCurrent = true;
	private BookBorrower dataSource;
	private boolean virtualPress= false;

	public BorrowerViewHolder(View itemView) {
		super(itemView);
		ButterKnife.bind(this, itemView);
	}

	public void bind(final BookBorrower toBind, final ClickHandler adapterHandler) {
		String booksBorrowed = toBind.getCurrentlyBorrowed() + "";
		personalData.setText(toBind.borrowerData());
		numberOfBooks.setText(booksBorrowed);

		dataSource = toBind;
		setupListeners(adapterHandler);
	}

	private void setupListeners(ClickHandler adapterHandler){
        itemView.setOnClickListener(v -> adapterHandler.onItemClicked(getAdapterPosition()));
        itemView.setOnLongClickListener(v -> {
            if(virtualPress)
                manageVirtualPress();
            adapterHandler.onItemLongClicked(getAdapterPosition());
            return false;
        });

        booksNumberContainer.setOnClickListener(v -> {
            String onClickText;
            if (numberOfCurrent) {
                numberOfCurrent = false;
                numberDescription.setText(v.getResources().getText(R.string.books_number_desc_alt));
                onClickText = dataSource.getAllBorrowed() + "";
                numberOfBooks.setText(onClickText);
            } else {
                numberOfCurrent = true;
                numberDescription.setText(v.getResources().getText(R.string.books_number_desc));
                onClickText = dataSource.getCurrentlyBorrowed() + "";
                numberOfBooks.setText(onClickText);
            }
        });

        booksNumberContainer.setOnLongClickListener(v -> {
            itemView.setPressed(true);
            virtualPress = true;
            itemView.performLongClick();
            return false;
        });
    }

    private void manageVirtualPress(){
        Handler pressHandler = new Handler();
        pressHandler.post(new Runnable() {
            @Override
            public void run() {
                if(!booksNumberContainer.isPressed()){
                    virtualPress = false;
                    itemView.setPressed(false);
                    pressHandler.removeCallbacks(this);
                }
                else
                    pressHandler.postDelayed(this, 100);
            }
        });
    }
}

