package sbtwski.librarian.adapters.holders;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.joda.time.DateTime;

import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import sbtwski.librarian.R;
import sbtwski.librarian.entities.BorrowedBook;
import sbtwski.librarian.handlers.ClickHandler;
import sbtwski.librarian.utils.TimeFormattingUtils;

import static sbtwski.librarian.utils.Constants.buttonClick;

public class BookViewHolder extends RecyclerView.ViewHolder{

	@BindView(R.id.book_title) TextView title;
	@BindView(R.id.book_author) TextView author;
	@BindView(R.id.borrow_date) TextView borrowDate;
	@BindView(R.id.borrow_description) TextView borrowDateDescription;
	@BindView(R.id.return_button) Button returnButton;
	@BindView(R.id.borrow_date_container) View borrowDateContainer;

	private boolean dateIsClicked = false;
	private DateTime borrowDateSource;
	private boolean virtualPress = false;

	public BookViewHolder(View itemView) {
		super(itemView);
		ButterKnife.bind(this, itemView);
	}

	public void bind(BorrowedBook toBind, ClickHandler adapterHandler) {
		title.setText(toBind.getTitle());
		author.setText(toBind.getAuthor());

		borrowDateSource = toBind.getBorrowDateTime();

		if (toBind.isBorrowed()) {
			borrowDate.setVisibility(View.VISIBLE);
			borrowDateDescription.setVisibility(View.VISIBLE);
			returnButton.setVisibility(View.VISIBLE);
			borrowDate.setText(TimeFormattingUtils.format(toBind.getBorrowDateTime()));
            returnButton.setOnClickListener(v -> {
                v.startAnimation(buttonClick);
                adapterHandler.onReturnClicked(getAdapterPosition());
            });
		} else {
			borrowDateContainer.setVisibility(View.GONE);
			returnButton.setVisibility(View.GONE);
		}

		setupListeners(adapterHandler);
	}

	private void setupListeners(ClickHandler adapterHandler) {
		itemView.setOnClickListener(v -> adapterHandler.onItemClicked(getAdapterPosition()));

		itemView.setOnLongClickListener(v -> {
		    if(virtualPress)
		        manageVirtualPress();
            adapterHandler.onItemLongClicked(getAdapterPosition());
            return false;
		});

		borrowDateContainer.setOnClickListener(v -> {
			if (borrowDateSource != null && returnButton.getVisibility() == View.VISIBLE) {
				if (!dateIsClicked) {
					String toPrint = countDays(itemView.getContext(), borrowDateSource.getMillis(), System.currentTimeMillis());
					borrowDate.setText(toPrint);
					borrowDateDescription.setText(v.getResources().getText(R.string.book_item_borrow_description_alt));
					dateIsClicked = true;
				} else {
					borrowDate.setText(TimeFormattingUtils.format(borrowDateSource));
					borrowDateDescription.setText(v.getResources().getText(R.string.book_item_borrow_description));
					dateIsClicked = false;
				}
			}
		});

		borrowDateContainer.setOnLongClickListener(v -> {
		    itemView.setPressed(true);
		    virtualPress = true;
            itemView.performLongClick();
		    return false;
        });
	}

	private void manageVirtualPress(){
        Handler pressHandler = new Handler();
        pressHandler.post(new Runnable() {
            @Override
            public void run() {
                if(!borrowDateContainer.isPressed()){
                    virtualPress = false;
                    itemView.setPressed(false);
                    pressHandler.removeCallbacks(this);
                }
                else
                    pressHandler.postDelayed(this, 100);
            }
        });
    }

	private static String countDays(Context context, long startDate, long endDate) {
		long millisDiff = endDate - startDate;
		long daysDiff = TimeUnit.MILLISECONDS.toDays(millisDiff);

		if (daysDiff == 1)
			return daysDiff + " " + context.getResources().getString(R.string.one_day);
		else
			return daysDiff + " " + context.getResources().getString(R.string.multiple_or_zero_days);
	}
}
