package sbtwski.librarian.adapters.holders;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import sbtwski.librarian.R;
import sbtwski.librarian.entities.BookBorrower;
import sbtwski.librarian.utils.TimeFormattingUtils;

public class BorrowerViewHolderInPreview extends RecyclerView.ViewHolder {
    private TextView borrowerPersonal;
    private TextView borrowPeriod;

    public BorrowerViewHolderInPreview(View itemView) {
        super(itemView);
        borrowerPersonal = itemView.findViewById(R.id.borrower_personal_data_book_preview);
        borrowPeriod = itemView.findViewById(R.id.borrow_period_book_preview);
    }

    public void bind(BookBorrower borrowing) {
        borrowerPersonal.setText(borrowing.borrowerData());
        borrowPeriod.setText(TimeFormattingUtils.formatPeriod(
                borrowing.getBorrowDateTime(),
                borrowing.getReturnDateTime()
        ));
    }
}