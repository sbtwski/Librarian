package sbtwski.librarian.adapters.holders;

import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;
import sbtwski.librarian.R;
import sbtwski.librarian.entities.BorrowedBook;
import sbtwski.librarian.utils.TimeFormattingUtils;

public class BookViewHolderInPreview extends RecyclerView.ViewHolder {

    private TextView title;
    private TextView author;
    private TextView borrowPeriod;

    public BookViewHolderInPreview(View itemView) {
        super(itemView);
        title = itemView.findViewById(R.id.book_title_borrower_preview);
        author = itemView.findViewById(R.id.book_author_borrower_preview);
        borrowPeriod = itemView.findViewById(R.id.borrow_period_borrower_preview);
    }

    public void bind(BorrowedBook borrowing) {
        title.setText(borrowing.getTitle());
        author.setText(borrowing.getAuthor());
        borrowPeriod.setText(TimeFormattingUtils.formatPeriod(
                borrowing.getBorrowDateTime(),
                borrowing.getReturnDateTime()
        ));
    }
}
