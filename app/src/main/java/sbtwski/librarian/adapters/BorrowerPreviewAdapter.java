package sbtwski.librarian.adapters;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import sbtwski.librarian.R;
import sbtwski.librarian.adapters.holders.BookViewHolderInPreview;
import sbtwski.librarian.entities.BorrowedBook;

public class BorrowerPreviewAdapter extends RecyclerView.Adapter<BookViewHolderInPreview> {

	private List<BorrowedBook> borrowings;

	public BorrowerPreviewAdapter() {
		this.borrowings = new ArrayList<>();
	}

	@Override
	public void onBindViewHolder(@NonNull BookViewHolderInPreview holder, int i) {
		holder.bind(borrowings.get(i));
	}

	@Override
	@NonNull
	public BookViewHolderInPreview onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
		LayoutInflater inflater = LayoutInflater.from(parent.getContext());
		View view = inflater.inflate(R.layout.books_list_item_borrower_preview, parent, false);
		return new BookViewHolderInPreview(view);
	}

	@Override
	public int getItemCount() {
		return borrowings.size();
	}

	public void setDatabase(List<BorrowedBook> database) {
		if(database != null) {
			this.borrowings = database;
			notifyDataSetChanged();
		}
	}
}
