package sbtwski.librarian.entities;

import java.io.Serializable;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

@Entity(tableName = "books", indices = {@Index(value = {"title", "author"}, unique = true)})
public class Book implements Serializable {

	@PrimaryKey(autoGenerate = true)
	private Long id;

	@NonNull
	private String title;

	@NonNull
	private String author;

	public Book(@NonNull String title,@NonNull String author) {
		this.title = title;
		this.author = author;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	@NonNull
	public String getTitle() {
		return title;
	}

	@NonNull
	public String getAuthor() {
		return author;
	}
}
