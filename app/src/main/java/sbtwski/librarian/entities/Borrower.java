package sbtwski.librarian.entities;

import java.io.Serializable;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

@Entity(tableName = "borrowers", indices = {@Index(value = {"name", "surname"}, unique = true)})
public class Borrower implements Serializable  {

	@PrimaryKey(autoGenerate = true)
	private Long id;

	@NonNull
	private String name;

	@NonNull
	private String surname;

	public Borrower(@NonNull String name,@NonNull String surname) {
		this.name = name;
		this.surname = surname;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	@NonNull
	public String getName() {
		return name;
	}

	@NonNull
	public String getSurname() {
		return surname;
	}

	@Override
    @NonNull
	public String toString() {
		return name + " " + surname;
	}
}
