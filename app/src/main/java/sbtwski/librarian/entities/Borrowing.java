package sbtwski.librarian.entities;

import org.joda.time.DateTime;

import java.io.Serializable;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Index;
import androidx.room.PrimaryKey;

@Entity(tableName = "borrowings",
		foreignKeys = {@ForeignKey(entity = Book.class, parentColumns = "id", childColumns = "book_id", onDelete = ForeignKey.CASCADE),
 					   @ForeignKey(entity = Borrower.class, parentColumns = "id", childColumns = "borrower_id", onDelete = ForeignKey.CASCADE)},
		indices = {@Index(value = "book_id"), @Index(value = "borrower_id")})
public class Borrowing implements Serializable  {

	@PrimaryKey(autoGenerate = true)
	private Long id;

	@ColumnInfo(name = "book_id")
	@NonNull
	private Long book;

	@ColumnInfo(name = "borrower_id")
	@NonNull
	private Long borrower;

	@NonNull
	private Long borrowTime;

	private Long returnTime;

	public Borrowing(@NonNull Long book,@NonNull Long borrower,@NonNull Long borrowTime, Long returnTime) {
		this.book = book;
		this.borrower = borrower;
		this.borrowTime = borrowTime;
		this.returnTime = returnTime;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	@NonNull
	public Long getBook() {
		return book;
	}

	@NonNull
	public Long getBorrower() {
		return borrower;
	}

	@NonNull
	public Long getBorrowTime() {
		return borrowTime;
	}

	public Long getReturnTime() {
		return returnTime;
	}

	public void setReturnTime(Long returnTime) {
		this.returnTime = returnTime;
	}

	@NonNull
	public DateTime getBorrowDateTime() {
		return new DateTime(borrowTime);
	}

	public DateTime getReturnDateTime() {
		return returnTime == null ? null : new DateTime(returnTime);
	}
}
