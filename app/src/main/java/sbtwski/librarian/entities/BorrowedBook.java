package sbtwski.librarian.entities;

import org.joda.time.DateTime;

public class BorrowedBook{
    private Long id;
    private String author;
    private String title;
    private Long borrowTime;
    private Long returnTime;
    private Long allBorrowings;

    public BorrowedBook(Long id, String author, String title, Long borrowTime, Long returnTime, Long allBorrowings) {
        this.id = id;
        this.author = author;
        this.title = title;
        this.borrowTime = borrowTime;
        this.returnTime = returnTime;
        this.allBorrowings = allBorrowings;
    }

    public Long getId() {
        return id;
    }

    public String getAuthor() {
        return author;
    }

    public String getTitle() {
        return title;
    }

    public Long getBorrowTime() {
        return borrowTime;
    }

    public Long getReturnTime() {
        return returnTime;
    }

    public Long getAllBorrowings() {
        return allBorrowings;
    }

    public boolean isBorrowed() {
        return borrowTime != null;
    }

    public DateTime getBorrowDateTime() {
        return new DateTime(borrowTime);
    }

    public DateTime getReturnDateTime() {
        return returnTime == null ? null : new DateTime(returnTime);
    }
}
