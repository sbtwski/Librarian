package sbtwski.librarian.entities;

import org.joda.time.DateTime;

public class BookBorrower{
    private Long id;
    private String name;
    private String surname;
    private Long borrowTime;
    private Long returnTime;
    private Long currentlyBorrowed;
    private Long allBorrowed;

    public BookBorrower(Long id, String name, String surname, Long borrowTime, Long returnTime, Long currentlyBorrowed, Long allBorrowed) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.borrowTime = borrowTime;
        this.returnTime = returnTime;
        this.currentlyBorrowed = currentlyBorrowed;
        this.allBorrowed = allBorrowed;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public Long getBorrowTime() {
        return borrowTime;
    }

    public Long getReturnTime() {
        return returnTime;
    }

    public String borrowerData(){
        return name + " " + surname;
    }

    public Long getCurrentlyBorrowed() {
        return currentlyBorrowed;
    }

    public Long getAllBorrowed() { return allBorrowed; }

    public DateTime getBorrowDateTime() {
        return new DateTime(borrowTime);
    }

    public DateTime getReturnDateTime() {
        return returnTime == null ? null : new DateTime(returnTime);
    }
}
