package sbtwski.librarian.entities;

import org.joda.time.DateTime;

public class BorrowPeriod{
    private Long borrowTime;
    private Long returnTime;

    public BorrowPeriod(Long borrowTime, Long returnTime) {
        this.borrowTime = borrowTime;
        this.returnTime = returnTime;
    }

    public Long getBorrowTime() {
        return borrowTime;
    }

    public Long getReturnTime() {
        return returnTime;
    }

    public DateTime getBorrowDateTime() {
        return new DateTime(borrowTime);
    }

    public DateTime getReturnDateTime() {
        return returnTime == null ? null : new DateTime(returnTime);
    }
}
