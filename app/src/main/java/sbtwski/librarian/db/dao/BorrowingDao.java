package sbtwski.librarian.db.dao;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;
import sbtwski.librarian.entities.BookBorrower;
import sbtwski.librarian.entities.BorrowPeriod;
import sbtwski.librarian.entities.BorrowedBook;
import sbtwski.librarian.entities.Borrowing;

@Dao
public interface BorrowingDao {

    @Insert
    void insert(Borrowing toInsert);

    @Update
    void updateBorrowing(Borrowing updated);

    @Query("SELECT * FROM borrowings")
    LiveData<List<Borrowing>> getAll();

    @Query("SELECT BO.id, BO.author, BO.title, BR.borrowTime, BR.returnTime, 0 AS allBorrowings " +
           "FROM borrowings BR JOIN books BO ON BR.book_id = BO.id " +
           "WHERE BR.borrower_id = :borrowerID")
    LiveData<List<BorrowedBook>> getBorrowedBooks(Long borrowerID);

    @Query("SELECT BO.id, BO.name, BO.surname, BR.borrowTime, BR.returnTime, 0 AS currentlyBorrowed, 0 AS allBorrowed " +
           "FROM borrowings BR JOIN borrowers BO ON BR.borrower_id = BO.id " +
           "WHERE BR.book_id = :bookID")
    LiveData<List<BookBorrower>> getBookBorrowers(Long bookID);

    @Query("SELECT BO.id, BO.author, BO.title, BR.borrowTime, BR.returnTime, allBorrowings " +
           "FROM books BO LEFT JOIN borrowings BR ON (BO.id = BR.book_id AND BR.returnTime IS NULL) " +
           "JOIN (SELECT book_id AS allID, COUNT(*) AS allBorrowings FROM borrowings GROUP BY book_id) ON allID = BO.id " +
           "GROUP BY BO.id")
    LiveData<List<BorrowedBook>> getAllBooks();

    @Query("SELECT BO.id, BO.name, BO.surname, BR.borrowTime, BR.returnTime, COUNT(BR.borrower_id) AS currentlyBorrowed, allBorrowed " +
           "FROM borrowers BO LEFT JOIN borrowings BR ON (BO.id = BR.borrower_id AND BR.returnTime IS NULL) " +
           "JOIN (SELECT borrower_id AS allID, COUNT(*) AS allBorrowed FROM borrowings GROUP BY borrower_id) ON allID = BO.id " +
           "GROUP BY BO.id")
    LiveData<List<BookBorrower>> getAllBorrowers();

    @Query("SELECT BR.borrowTime, BR.returnTime " +
           "FROM borrowings BR JOIN books BO ON BR.book_id = BO.id " +
           "WHERE BO.title = :bookTitle AND BO.author = :bookAuthor")
    LiveData<List<BorrowPeriod>> getBookBorrowPeriods(String bookTitle, String bookAuthor);

    @Query("SELECT * FROM borrowings WHERE book_id = :bookID AND returnTime IS NULL")
    LiveData<Borrowing> getBookBorrowing(Long bookID);
}
