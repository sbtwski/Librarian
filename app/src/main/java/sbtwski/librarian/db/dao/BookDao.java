package sbtwski.librarian.db.dao;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;
import sbtwski.librarian.entities.Book;

@Dao
public interface BookDao {

    @Insert
    void insert(Book toInsert);

    @Update
    void updateBook(Book updated);

    @Query("SELECT * FROM books")
    LiveData<List<Book>> getAll();

    @Query("SELECT * FROM books WHERE id = :bookID")
    LiveData<Book> getByID(Long bookID);

    @Query("SELECT id FROM books WHERE title = :bookTitle AND author = :bookAuthor")
    LiveData<Long> getID(String bookTitle, String bookAuthor);

    @Query("DELETE FROM books WHERE id = :bookID")
    void deleteRow(Long bookID);
}
