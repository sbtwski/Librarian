package sbtwski.librarian.db.dao;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;
import sbtwski.librarian.entities.Borrower;

@Dao
public interface BorrowerDao {

    @Insert
    void insert(Borrower toInsert);

    @Update
    void updateBorrower(Borrower updated);

    @Query("SELECT * FROM borrowers")
    LiveData<List<Borrower>> getAll();

    @Query("SELECT * FROM borrowers WHERE id = :borrowerID")
    LiveData<Borrower> getByID(Long borrowerID);

    @Query("SELECT id FROM borrowers WHERE name = :borrowerName AND surname = :borrowerSurname")
    LiveData<Long> getID(String borrowerName, String borrowerSurname);

    @Query("DELETE FROM borrowers WHERE id = :borrowerID")
    void deleteRow(Long borrowerID);
}
