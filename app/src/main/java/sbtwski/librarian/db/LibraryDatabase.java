package sbtwski.librarian.db;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import sbtwski.librarian.db.dao.BookDao;
import sbtwski.librarian.db.dao.BorrowerDao;
import sbtwski.librarian.db.dao.BorrowingDao;
import sbtwski.librarian.entities.Book;
import sbtwski.librarian.entities.Borrower;
import sbtwski.librarian.entities.Borrowing;

@Database(entities = {Book.class, Borrower.class, Borrowing.class}, version = 1, exportSchema = false)
public abstract class LibraryDatabase extends RoomDatabase {

    public abstract BookDao bookDao();
    public abstract BorrowerDao borrowerDao();
    public abstract BorrowingDao borrowingDao();

    private static volatile LibraryDatabase INSTANCE;

    public static LibraryDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (LibraryDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                               LibraryDatabase.class, "libraryDB")
                               .allowMainThreadQueries()
                               .build();
                }
            }
        }
        return INSTANCE;
    }
}
