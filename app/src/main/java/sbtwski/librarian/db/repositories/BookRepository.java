package sbtwski.librarian.db.repositories;

import android.app.Application;
import android.database.sqlite.SQLiteConstraintException;
import android.os.AsyncTask;
import android.util.Log;

import java.util.List;

import androidx.lifecycle.LiveData;
import sbtwski.librarian.db.LibraryDatabase;
import sbtwski.librarian.db.dao.BookDao;
import sbtwski.librarian.entities.Book;

public class BookRepository {

    private BookDao bookDao;
    private LiveData<List<Book>> allBooks;

    public BookRepository(Application application) {
        LibraryDatabase db = LibraryDatabase.getDatabase(application);
        bookDao = db.bookDao();
        allBooks = bookDao.getAll();
    }

    public LiveData<List<Book>> getAllBooks() {
        return allBooks;
    }

    public void insert (Book toInsert) {
        new insertAsyncTask(bookDao).execute(toInsert);
    }

    public void deleteRow(Long deleteID) { new deleteAsyncTask(bookDao).execute(deleteID); }

    public void updateRow(Book updated) { new updateAsyncTask(bookDao).execute(updated); }

    public LiveData<Long> getID(Book searched){
        return bookDao.getID(searched.getTitle(), searched.getAuthor());
    }

    public LiveData<Book> getByID(Long bookID) { return bookDao.getByID(bookID); }

    private static class insertAsyncTask extends AsyncTask<Book, Void, Void> {

        private BookDao asyncTaskDao;

        insertAsyncTask(BookDao dao) {
            asyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Book... params) {
            try {
                asyncTaskDao.insert(params[0]);
            }
            catch (SQLiteConstraintException e){
                Log.i("SQL_insert", "Book already in DB");
            }
            return null;
        }
    }

    private static class deleteAsyncTask extends AsyncTask<Long, Void, Void> {

        private BookDao asyncTaskDao;

        deleteAsyncTask(BookDao dao) {
            asyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Long... params) {
            asyncTaskDao.deleteRow(params[0]);
            return null;
        }
    }

    private static class updateAsyncTask extends AsyncTask<Book, Void, Void> {

        private BookDao asyncTaskDao;

        updateAsyncTask(BookDao dao) {
            asyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Book... params) {
            asyncTaskDao.updateBook(params[0]);
            return null;
        }
    }
}
