package sbtwski.librarian.db.repositories;

import android.app.Application;
import android.database.sqlite.SQLiteConstraintException;
import android.os.AsyncTask;
import android.util.Log;

import java.util.List;

import androidx.lifecycle.LiveData;
import sbtwski.librarian.db.LibraryDatabase;
import sbtwski.librarian.db.dao.BorrowerDao;
import sbtwski.librarian.entities.Borrower;

public class BorrowerRepository {

    private BorrowerDao borrowerDao;
    private LiveData<List<Borrower>> allBorrowers;

    public BorrowerRepository(Application application) {
        LibraryDatabase db = LibraryDatabase.getDatabase(application);
        borrowerDao = db.borrowerDao();
        allBorrowers = borrowerDao.getAll();
    }

    public LiveData<List<Borrower>> getAllBorrowers() {
        return allBorrowers;
    }

    public void insert (Borrower toInsert) { new insertAsyncTask(borrowerDao).execute(toInsert); }

    public void deleteRow(Long deleteID) { new deleteAsyncTask(borrowerDao).execute(deleteID); }

    public void updateRow(Borrower updated) { new updateAsyncTask(borrowerDao).execute(updated); }

    public LiveData<Long> getID(Borrower searched){
        return borrowerDao.getID(searched.getName(), searched.getSurname());
    }

    public LiveData<Borrower> getByID(Long borrowerID) { return borrowerDao.getByID(borrowerID); }

    private static class insertAsyncTask extends AsyncTask<Borrower, Void, Void> {

        private BorrowerDao asyncTaskDao;

        insertAsyncTask(BorrowerDao dao) {
            asyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Borrower... params) {
            try {
                asyncTaskDao.insert(params[0]);
            }
            catch (SQLiteConstraintException e){
                Log.i("SQL_insert", "Borrower already in DB");
            }
            return null;
        }
    }

    private static class deleteAsyncTask extends AsyncTask<Long, Void, Void> {

        private BorrowerDao asyncTaskDao;

        deleteAsyncTask(BorrowerDao dao) {
            asyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Long... params) {
            asyncTaskDao.deleteRow(params[0]);
            return null;
        }
    }

    private static class updateAsyncTask extends AsyncTask<Borrower, Void, Void> {

        private BorrowerDao asyncTaskDao;

        updateAsyncTask(BorrowerDao dao) {
            asyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Borrower... params) {
            asyncTaskDao.updateBorrower(params[0]);
            return null;
        }
    }
}
