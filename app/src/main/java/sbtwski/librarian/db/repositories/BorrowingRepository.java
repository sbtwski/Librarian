package sbtwski.librarian.db.repositories;

import android.app.Application;
import android.os.AsyncTask;

import java.util.List;

import androidx.lifecycle.LiveData;
import sbtwski.librarian.db.LibraryDatabase;
import sbtwski.librarian.db.dao.BorrowingDao;
import sbtwski.librarian.entities.Book;
import sbtwski.librarian.entities.BookBorrower;
import sbtwski.librarian.entities.BorrowPeriod;
import sbtwski.librarian.entities.BorrowedBook;
import sbtwski.librarian.entities.Borrower;
import sbtwski.librarian.entities.Borrowing;

public class BorrowingRepository {

    private BorrowingDao borrowingDao;
    private LiveData<List<Borrowing>> allBorrowings;

    public BorrowingRepository(Application application) {
        LibraryDatabase db = LibraryDatabase.getDatabase(application);
        borrowingDao = db.borrowingDao();
        allBorrowings = borrowingDao.getAll();
    }

    public LiveData<List<Borrowing>> getAllBorrowings() {
        return allBorrowings;
    }

    public LiveData<List<BookBorrower>> getBookBorrowers(Book book) { return borrowingDao.getBookBorrowers(book.getId()); }

    public LiveData<List<BorrowedBook>> getBorrowedBooks(Borrower borrower) { return borrowingDao.getBorrowedBooks(borrower.getId()); }

    public LiveData<List<BookBorrower>> getAllBorrowers() { return borrowingDao.getAllBorrowers(); }

    public LiveData<List<BorrowedBook>> getAllBooks() { return borrowingDao.getAllBooks(); }

    public LiveData<List<BorrowPeriod>> getBookBorrowPeriods(Book toCheck) { return borrowingDao.getBookBorrowPeriods(toCheck.getTitle(), toCheck.getAuthor()); }

    public LiveData<Borrowing> getBookBorrowing(Long bookID) { return borrowingDao.getBookBorrowing(bookID); }

    public void insert (Borrowing toInsert) {
        new insertAsyncTask(borrowingDao).execute(toInsert);
    }

    public void update (Borrowing toUpdate) { new updateAsyncTask(borrowingDao).execute(toUpdate); }

    private static class insertAsyncTask extends AsyncTask<Borrowing, Void, Void> {

        private BorrowingDao asyncTaskDao;

        insertAsyncTask(BorrowingDao dao) {
            asyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Borrowing... params) {
            asyncTaskDao.insert(params[0]);
            return null;
        }
    }

    private static class updateAsyncTask extends AsyncTask<Borrowing, Void, Void> {

        private BorrowingDao asyncTaskDao;

        updateAsyncTask(BorrowingDao dao) {
            asyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Borrowing... params) {
            asyncTaskDao.updateBorrowing(params[0]);
            return null;
        }
    }
}
