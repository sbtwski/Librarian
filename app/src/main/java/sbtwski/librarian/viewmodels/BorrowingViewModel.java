package sbtwski.librarian.viewmodels;

import android.app.Application;

import java.util.List;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import sbtwski.librarian.db.repositories.BorrowingRepository;
import sbtwski.librarian.entities.Book;
import sbtwski.librarian.entities.BookBorrower;
import sbtwski.librarian.entities.BorrowPeriod;
import sbtwski.librarian.entities.BorrowedBook;
import sbtwski.librarian.entities.Borrower;
import sbtwski.librarian.entities.Borrowing;

public class BorrowingViewModel extends AndroidViewModel {
    private BorrowingRepository repository;

    private LiveData<List<Borrowing>> allBorrowings;

    public BorrowingViewModel (Application application) {
        super(application);
        repository = new BorrowingRepository(application);
        allBorrowings = repository.getAllBorrowings();
    }

    public LiveData<List<Borrowing>> getAllBorrowings() { return allBorrowings; }

    public LiveData<List<BookBorrower>> getBookBorrowers(Book book) { return repository.getBookBorrowers(book); }

    public LiveData<List<BorrowedBook>> getBorrowedBooks(Borrower borrower) { return repository.getBorrowedBooks(borrower); }

    public LiveData<List<BookBorrower>> getAllBorrowers() { return repository.getAllBorrowers(); }

    public LiveData<List<BorrowedBook>> getAllBooks() { return repository.getAllBooks(); }

    public LiveData<List<BorrowPeriod>> getBookBorrowPeriods(Book toCheck) { return repository.getBookBorrowPeriods(toCheck); }

    public LiveData<Borrowing> getBookBorrowing(Long bookID) { return repository.getBookBorrowing(bookID); }

    public void insert(Borrowing toInsert) { repository.insert(toInsert); }

    public void update(Borrowing toUpdate) { repository.update(toUpdate); }
}
