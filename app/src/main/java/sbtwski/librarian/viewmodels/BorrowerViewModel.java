package sbtwski.librarian.viewmodels;

import android.app.Application;

import java.util.List;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import sbtwski.librarian.db.repositories.BorrowerRepository;
import sbtwski.librarian.entities.Borrower;

public class BorrowerViewModel extends AndroidViewModel {
    private BorrowerRepository repository;

    private LiveData<List<Borrower>> allBorrowers;

    public BorrowerViewModel (Application application) {
        super(application);
        repository = new BorrowerRepository(application);
        allBorrowers = repository.getAllBorrowers();
    }

    public LiveData<List<Borrower>> getAllBorrowers() { return allBorrowers; }

    public LiveData<Long> getID(Borrower searched) { return repository.getID(searched); }

    public LiveData<Borrower> getByID(Long borrowerID) { return repository.getByID(borrowerID); }

    public void insert(Borrower toInsert) { repository.insert(toInsert); }

    public void deleteRow(Long deleteID) { repository.deleteRow(deleteID); }

    public void updateRow(Borrower updated) { repository.updateRow(updated); }
}
