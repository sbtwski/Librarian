package sbtwski.librarian.viewmodels;

import android.app.Application;

import java.util.List;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import sbtwski.librarian.db.repositories.BookRepository;
import sbtwski.librarian.entities.Book;

public class BookViewModel extends AndroidViewModel {
    private BookRepository repository;

    private LiveData<List<Book>> allBooks;

    public BookViewModel (Application application) {
        super(application);
        repository = new BookRepository(application);
        allBooks = repository.getAllBooks();
    }

    public LiveData<List<Book>> getAllBooks() { return allBooks; }

    public LiveData<Long> getID(Book searched) { return repository.getID(searched); }

    public LiveData<Book> getByID(Long bookID) { return repository.getByID(bookID); }

    public void insert(Book toInsert) { repository.insert(toInsert); }

    public void deleteRow(Long deleteID) { repository.deleteRow(deleteID); }

    public void updateRow(Book updated) { repository.updateRow(updated); }
}
